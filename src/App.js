import React, {useState, useEffect, useCallback} from 'react';

import {TableRow} from './components/TableRow';
import {TableTh} from './components/TableTh';
import './fonts/fonts.css';
import './App.scss';

const API_URL = 'https://reqres.in/api/unknown?per_page=12';

function App() {
    const [data, setData] = useState([]);
    const [columns, setColumns] = useState([]);
    const [visibleColumns, setVisibleColumns] = useState([]);
    const [numericColumns, setNumericColumns] = useState([]);

    useEffect(() => {
        fetch(API_URL)
            .then(response => response.json())
            .then(json => setData(json.data));
    }, []);

    useEffect(() => {
        if (data[0]) {
            setColumns(Object.keys(data[0]).map((item, index) => ({
                id: index,
                name: item,
            })));

            setNumericColumns(Object.keys(data[0]).filter(item => !isNaN(Number(data[0][item]))));
        }
    }, [data]);

    const resetVisible = useCallback((force = false) => {
        const savedColumns = localStorage.getItem("savedColumns");
        const newVisibleColumns = (savedColumns && !force) ?
            savedColumns.split(",") :
            columns.map(column => column.name);
        setVisibleColumns(newVisibleColumns);
        localStorage.setItem("savedColumns", newVisibleColumns.toString());
    }, [columns]);

    useEffect(() => {
        resetVisible();
    }, [resetVisible]);

    const changeVisible = (column) => {
        const newVisibleColumns = visibleColumns.filter(col => col !== column.name);
        setVisibleColumns(newVisibleColumns);
        localStorage.setItem("savedColumns", newVisibleColumns.toString());
    }

    return (
        <div className="app">
            <div className="b-content">
                <h1 className="b-content__title">Pantone colors</h1>
                <button
                    className="b-content__reset"
                    onClick={() => resetVisible(true)}
                    disabled={columns.length === visibleColumns.length}
                >Reset</button>
                <div className="b-table">
                    <div className="b-table__tr">
                        {
                            columns && columns.map(column =>
                                <TableTh
                                    key={column.id}
                                    column={column}
                                    isVisible={visibleColumns.includes(column.name)}
                                    isNumeric={numericColumns.includes(column.name)}
                                    changeVisible={changeVisible}
                                />
                            )
                        }
                    </div>
                    {
                        data && data.map((row, index) =>
                            <TableRow
                                key={row.id}
                                data={row}
                                isLast={index === data.length - 1}
                                visibleColumns={visibleColumns}
                            />
                        )
                    }
                </div>
            </div>
        </div>
    );
}

export default App;
