import React from "react";
import classNames from 'classnames/bind';

export const TableTh = (({column, isVisible, isNumeric, changeVisible}) => <div
        className={
            classNames(
                "b-table__th",
                {"b-table__td_right": isNumeric},
                {"b-table__td_invisible": !isVisible}
            )
        }
    >
        <input
            type="checkbox"
            checked={true}
            onChange={() => {
                changeVisible(column)
            }}
        />
        {column.name}
    </div>
);