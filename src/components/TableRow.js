import React from "react";
import classNames from 'classnames/bind';

export const TableRow = (({data, isLast, visibleColumns}) =>
        <div className={classNames("b-table__tr", {"b-table__tr_last": isLast})}>
            <div className={
                classNames(
                    "b-table__td",
                    "b-table__td_right",
                    {"b-table__td_invisible": !visibleColumns.includes("id")}
                )}
            > {data.id}</div>
            <div className={
                classNames(
                    "b-table__td",
                    "b-table__td_capitalize",
                    {"b-table__td_invisible": !visibleColumns.includes("name")}
                )}
            >{data.name}</div>
            <div className={
                classNames(
                    "b-table__td",
                    "b-table__td_right",
                    {"b-table__td_invisible": !visibleColumns.includes("year")}
                )}
            >{data.year}</div>
            <div className={
                classNames(
                    "b-table__td",
                    {"b-table__td_invisible": !visibleColumns.includes("color")}
                )}
            >
                <div className="b-table__color-preview" style={{background: data.color}}></div>
                {data.color}
            </div>
            <div className={
                classNames(
                    "b-table__td",
                    {"b-table__td_invisible": !visibleColumns.includes("pantone_value")}
                )}
            >{data.pantone_value}</div>
        </div>
);